const configJSON =
  {
    exchanges: [{value: 1, name: "test", desc: "testdesc"}],
    supports_search: true,
    supports_time: false,
    supports_timescale_marks: true,
    supports_group_request: false,
    supports_marks: true,
    supported_resolutions: ['1', '5', '15', '30', '60', '120', '240','1D', '1W'],

    /*exchanges: [{value: 1, name: "test", desc: "testdesc"}],
    symbols_types: [],
    supported_resolutions: [1, 5, 15, 30, 60, "D", "W"],
    supports_marks: true,
    supports_timescale_marks: true,
    supports_time: false*/
  };

const symbolResolveJSON =
  {
    name: '',
    'exchange-traded': '',
    'exchange-listed': '',
    timezone: 'Asia/Shanghai',
    minmov: 1,
    minmov2: 0,
    pointvalue: 1,
    session: '0930-1600',
    has_seconds: true,
    // "seconds_multipliers": ["1S", "5S", "15S", "30S"],
    has_intraday: true,
    intraday_multipliers: ['1', '5', '15', '30', '60', '120', '240','1D', '1W'],
    has_daily: true,
    has_empty_bars: false,
    has_no_volume: false,
    has_weekly_and_monthly: true,
    description: '',
    type: 'Index',
    supported_resolutions: ['1', '5', '15', '30', '60', '120', '240', '360', '720', '1D', '1W', '1M'],
    pricescale: 100,
    ticker: 'BTC/USDT2',
  };
export default {
  configJSON,
  symbolResolveJSON,
  // originSymbolResolveJSON
};
