import Vue from 'vue'

/**
 * @param num
 * @returns {*}
 */
function checkTime(num){
  if(num<10){
    return "0"+num
  }else{
    return num
  }
}

/**
 * zxd
 * type:long 代表获取完整的年月日时分秒，否则代表获取年月日(默认)
 */ 
Vue.filter("normalTime", function(value,type) {
  if (value) {
    let oDate = new Date();
    oDate.setTime(value);
    let y = oDate.getFullYear();
    let m = oDate.getMonth() + 1;
    let d = oDate.getDate();

    let h = oDate.getHours();
    let mm = oDate.getMinutes();
    let s = oDate.getSeconds();
    if(type=="long"){
      return y + '-' + checkTime(m) + '-' + checkTime(d) + ' ' + checkTime(h) + ':' + checkTime(mm) + ':' + checkTime(s);
    }else{
      return y + '.' + checkTime(m) + '.' + checkTime(d);
    }
  }
})

/**
 * 格式化金额
 */
Vue.filter("formatMoneyNumber", function(moneyNum) {
  if(typeof(moneyNum=="number")){
    moneyNum=moneyNum+"";
  }
  var moneyNum = moneyNum.replace(/,/g,"");
  //var result = isNaN(1 * moneyNum) ? new Number(0) : (1 * moneyNum);
  return /\./.test(moneyNum) ? moneyNum.replace(/(\d{1,3})(?=(\d{3})+\.)/g, "$1,") : moneyNum.replace(/(\d{1,3})(?=(\d{3})+\b)/g, "$1,");
})

/**
 * 处理小数点位数
 */
Vue.filter("toRoundDown", function(number,scaleAmount){
  if(number==null){
    return "--.--"
  }
  //因为科学计数法处理的方法入参必须为字符串
  var numStr = number + "";
  if(numStr.indexOf("e") != -1){
    numStr = scienceNum(number)
    number = numStr
  }
  var index = (numStr).indexOf(".");
  if(index == -1){
    //number为整数
    if(scaleAmount>0){
      var endStr = "."
      for(var i = 0;i<scaleAmount;i++){
        endStr += "0"
      }
    }else{
      var endStr = ""
    }
    return  number + endStr
  }else{
    //number不为整数
    var endStr = (number+"").slice(index+1)
    var startStr = (number+"").slice(0,index)
    //如果scaleAmount>0
    if(scaleAmount>0){
      //现有小数点位数 大于需要保留的位数  直接截取
      if(endStr.length>scaleAmount){
        return startStr+"."+endStr.slice(0,scaleAmount)
      }else{
        var len = scaleAmount - endStr.length;
        var endStr = ""
        if(len != 0){
          for(var j = 0;j<len;j++){
            endStr += "0"
          }
        }
        return  number + endStr
      }
    }else if(scaleAmount == 0){
      //如果scaleAmount = 0
      return  Math.floor(number)+""
    }
  }
})

/**
 * 科学计数法转换
 * @param value
 * @returns {*}
 */
function scienceNum(value) {
  if (isEmpty(value)) {
    return value;
  }
  value = value.toString();
  if (value.indexOf("e") > 0 || value.indexOf("E") > 0) {
    var ePart;
    var scalePart;
    var valueList;
    if (value.indexOf("e") > 0) {
      valueList = value.split("e");
    } else if (value.indexOf("E") > 0) {
      valueList = value.split("E");
    }
    if (valueList.length < 2) {
      return value;
    }
    ePart = valueList[0];
    scalePart = valueList[1];
    var result;
    if (ePart.indexOf(".") > 0) {
      var ePartList = ePart.split(".");
      var fLength = ePartList[0].length;
      var sLength = ePartList[1].length;
      if (scalePart.indexOf("-") >= 0) {
        if (ePartList[0].indexOf("-") >= 0) {
          ePartList[0] = ePartList[0].replace("-", "");
          result = "-0.";
        } else {
          result = "0.";
          ePartList[0] = ePartList[0].replace("+", "");
        }
        var scalePartList = scalePart.split("-");
        for (var i = 0; i < scalePartList[1] - 1; i++) {
          result = result + "0";
        }
        result = result + ePartList[0] + ePartList[1];
        return result;
      } else {
        var scalePartList;
        if (scalePart.indexOf("+") >= 0) {
          scalePartList = scalePart.split("+");
          scalePartList = scalePartList[1];
        } else {
          scalePartList = scalePart;
        }
        ePartList[0] = ePartList[0].replace("+", "");
        result = ePartList[0].toString() + ePartList[1].toString();
        var leftLength = scalePartList - sLength;
        if (leftLength > 0) {
          for (var j = 0; j < leftLength; j++) {
            result = result + "0";
          }
        }
        return result;

      }
    } else {
      if (scalePart.indexOf("-") >= 0) {
        if (ePart.indexOf("-") >= 0) {
          ePart = ePart.replace("-", "");
          result = "-0.";
        } else {
          ePart = ePart.replace("+", "");
          result = "0.";
        }
        var scalePartList = scalePart.split("-");
        for (var i = 0; i < scalePartList[1] - 1; i++) {
          result = result + "0";
        }
        result = result + ePart;
        return result;
      } else {
        var scalePartList;
        if (scalePart.indexOf("+") >= 0) {
          scalePartList = scalePart.split("+");
          scalePartList = scalePartList[1];
        } else {
          scalePartList = scalePart;
        }
        ePart = ePart.replace("+", "");
        result = ePart.toString();
        var leftLength = scalePartList;
        if (leftLength > 0) {
          for (var j = 0; j < leftLength; j++) {
            result = result + "0";
          }
        }
        return result;

      }
    }

  }
  return value;
}

function isEmpty(value) {
  return 'undefined' === typeof value || null === value || "" === value || "null" === value;
}
