
var string = function(){};
  /*zxd
   * 移除金额逗号分隔符
   * 入参：
   *      money：金额
   *
   * 返回：
   *      移除逗号后的金额
   */
  string.removeMoneyComma = function(money) {
    return money.replace(/,/g, "");
  };


  /*
    * 格式化金额数字，金额逗号分隔符
    *@author    zxd
    *
    * 入参：
    *       money：金额
    *
    * 返回：
    *      格式化后的金额
    */
  string.formatMoneyNumber =function(moneyNum) {

    if(typeof(moneyNum=="number")){
      moneyNum=moneyNum+"";
    }
    var moneyNum = moneyNum.replace(/,/g,"");
    //var result = isNaN(1 * moneyNum) ? new Number(0) : (1 * moneyNum);
    return /\./.test(moneyNum) ? moneyNum.replace(/(\d{1,3})(?=(\d{3})+\.)/g, "$1,") : moneyNum.replace(/(\d{1,3})(?=(\d{3})+\b)/g, "$1,");
  };

/*
  * 格式化金额数字，小数部分和整数部分用不同的样式
  *@author    zxd
  *
  * 入参：
  *      options{money：'',class1:'',class2:''}都为可选

  *
  * 返回：
  *      格式化后的金额html
  */
string.formatNumber=function(options){
  var money=options.money<0|| typeof options.money=='undefined' ?0:options.money;
  var class1= typeof options.class1=='undefined' ?"f4":options.class1;
  var class2= typeof options.class1=='undefined' ?"f2":options.class2;
  var formatMoney=string.formatMoneyNumber(money);
  var split=formatMoney.split(".");
  floor=split[0];
  decimal=split[1];
  var html="<span class=\""+class1+"\">"+floor+".</span>";
  html+="<span class=\""+class2+"\">"+decimal+"</span>";
  return html;
};

/**
 *  获取url地址栏上的参数
 * @param name
 * @returns {RegExpMatchArray | string[] | string | string}
 */
string.getParam = function () {  //获取参数
  /*var url = location.search,
    reg = new RegExp(name + '=[^&]*'),
    arr = url.match(reg),
    result = arr && arr.toString().split('=');
  return result && result[1] || '';*/
  var arr2;
  var search=location.search;  //获取location的search属性，保存在search中
  var params={};      //创建空对象params
  if(search!=""){     //如果search不是空字符串
    var arr=search.split("?");
    var arr1=arr[1].split("&");
    for(var i=0;i<arr1.length;i++){
      arr2=arr1[i].split('=');

      if(!arr2[1]){
        params[arr2[0]]='';
      }else{
        if(params[arr2[0]]){
          var arr3=[params[arr2[0]]];
          arr3.push(arr2[1]);
          params[arr2[0]] = arr3;
        } else {
          params[arr2[0]]=decodeURI(arr2[1]);
        }
      }
    }
  }
  return params;
}



//number类型转换成 有“+” “-” “%” 的形式 （固定小数点后两位） 如 “+0.00%”，“-0.30”(场景：涨跌幅)

string.toPercentage = function(str){
  if(str==null){
    return "+0.00%"
  }
  str = str + ""
  var arr = str.split(".");
  if(arr[1]){
    if(arr[1].length == 1){
      str = str + "0"
    }else if(arr[1].length > 2){
      str = arr[0] + "."+ arr[1].slice(0,2)
    }
  }else{
    str = str +".00"
  }
  if(str.indexOf("-") == -1){
    if(str.indexOf("+") == -1){
      str = "+" + str
    }
  }
  return str + "%"
}


//number类型转换成 带有固定小数位的字符串  向下取整  （场景：价格 金额 数量 等）
/**
 * lxz
 * @param number
 * @param scaleAmount
 * @returns {string}
 */
string.toRoundDown = function(number,scaleAmount){
  if(number==null){
    return "--.--"
  }
  //因为科学计数法处理的方法入参必须为字符串
  var numStr = number + "";
  if(numStr.indexOf("e") != -1){
    numStr = scienceNum(number)
    number = numStr
  }
  var index = (numStr).indexOf(".");
  if(index == -1){
    //number为整数
    if(scaleAmount>0){
      var endStr = "."
      for(var i = 0;i<scaleAmount;i++){
        endStr += "0"
      }
    }else{
      var endStr = ""
    }
    return  number + endStr
  }else{
    //number不为整数
    var endStr = (number+"").slice(index+1)
    var startStr = (number+"").slice(0,index)
    //如果scaleAmount>0
    if(scaleAmount>0){
      //现有小数点位数 大于需要保留的位数  直接截取
      if(endStr.length>scaleAmount){
        return startStr+"."+endStr.slice(0,scaleAmount)
      }else{
        var len = scaleAmount - endStr.length;
        var endStr = ""
        if(len != 0){
          for(var j = 0;j<len;j++){
            endStr += "0"
          }
        }
        return  number + endStr
      }
    }else if(scaleAmount == 0){
      //如果scaleAmount = 0
      if(number<0){
        return  (-1 * Math.floor(-1 * number)) + ""
      }else{
        return  Math.floor(number) + ""
      }
    }
  }
}

/**
 * 科学计数法转换
 * @param value
 * @returns {*}
 */
function scienceNum(value) {
  if (isEmpty(value)) {
    return value;
  }
  value = value.toString();
  if (value.indexOf("e") > 0 || value.indexOf("E") > 0) {
    var ePart;
    var scalePart;
    var valueList;
    if (value.indexOf("e") > 0) {
      valueList = value.split("e");
    } else if (value.indexOf("E") > 0) {
      valueList = value.split("E");
    }
    if (valueList.length < 2) {
      return value;
    }
    ePart = valueList[0];
    scalePart = valueList[1];
    var result;
    if (ePart.indexOf(".") > 0) {
      var ePartList = ePart.split(".");
      var fLength = ePartList[0].length;
      var sLength = ePartList[1].length;
      if (scalePart.indexOf("-") >= 0) {
        if (ePartList[0].indexOf("-") >= 0) {
          ePartList[0] = ePartList[0].replace("-", "");
          result = "-0.";
        } else {
          result = "0.";
          ePartList[0] = ePartList[0].replace("+", "");
        }
        var scalePartList = scalePart.split("-");
        for (var i = 0; i < scalePartList[1] - 1; i++) {
          result = result + "0";
        }
        result = result + ePartList[0] + ePartList[1];
        return result;
      } else {
        var scalePartList;
        if (scalePart.indexOf("+") >= 0) {
          scalePartList = scalePart.split("+");
          scalePartList = scalePartList[1];
        } else {
          scalePartList = scalePart;
        }
        ePartList[0] = ePartList[0].replace("+", "");
        result = ePartList[0].toString() + ePartList[1].toString();
        var leftLength = scalePartList - sLength;
        if (leftLength > 0) {
          for (var j = 0; j < leftLength; j++) {
            result = result + "0";
          }
        }
        return result;

      }
    } else {
      if (scalePart.indexOf("-") >= 0) {
        if (ePart.indexOf("-") >= 0) {
          ePart = ePart.replace("-", "");
          result = "-0.";
        } else {
          ePart = ePart.replace("+", "");
          result = "0.";
        }
        var scalePartList = scalePart.split("-");
        for (var i = 0; i < scalePartList[1] - 1; i++) {
          result = result + "0";
        }
        result = result + ePart;
        return result;
      } else {
        var scalePartList;
        if (scalePart.indexOf("+") >= 0) {
          scalePartList = scalePart.split("+");
          scalePartList = scalePartList[1];
        } else {
          scalePartList = scalePart;
        }
        ePart = ePart.replace("+", "");
        result = ePart.toString();
        var leftLength = scalePartList;
        if (leftLength > 0) {
          for (var j = 0; j < leftLength; j++) {
            result = result + "0";
          }
        }
        return result;

      }
    }

  }
  return value;
}

function isEmpty(value) {
  return 'undefined' === typeof value || null === value || "" === value || "null" === value;
}


//js 浮点数 加法  bug
/**
 *zn
 * @param arg1
 * @param arg2
 * @returns {number}
 */
string.floatAdd = function (arg1,arg2) {
  var r1, r2, m, c;
  try {
      r1 = arg1.toString().split(".")[1].length;
  }
  catch (e) {
      r1 = 0;
  }
  try {
      r2 = arg2.toString().split(".")[1].length;
  }
  catch (e) {
      r2 = 0;
  }
  c = Math.abs(r1 - r2);
  m = Math.pow(10, Math.max(r1, r2));
  if (c > 0) {
      var cm = Math.pow(10, c);
      if (r1 > r2) {
          arg1 = Number(arg1.toString().replace(".", ""));
          arg2 = Number(arg2.toString().replace(".", "")) * cm;
      } else {
          arg1 = Number(arg1.toString().replace(".", "")) * cm;
          arg2 = Number(arg2.toString().replace(".", ""));
      }
  } else {
      arg1 = Number(arg1.toString().replace(".", ""));
      arg2 = Number(arg2.toString().replace(".", ""));
  }
  return (arg1 + arg2) / m;
  // 以下为弃用方法
  // var r1,r2,m;
  // try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}
  // try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}
  // m=Math.pow(10,Math.max(r1,r2));
  // return (arg1*m+arg2*m)/m;
}


//js 浮点数 减法  bug
/**
 * zn
 * @param arg1
 * @param arg2
 * @returns {string}
 */
string.floatSub = function (arg1,arg2) {
  var r1,r2,m,n;
  try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}
  try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}
  m=Math.pow(10,Math.max(r1,r2));
  //动态控制精度长度
  n=(r1>=r2)?r1:r2;
  return ((arg1*m-arg2*m)/m).toFixed(n);
}

//解决 js 浮点数 乘法  bug
/**
 * zn
 * @param arg1
 * @param arg2
 * @returns {number}
 */
string.floatMul = function(arg1,arg2)   {
  var m=0,s1=arg1.toString(),s2=arg2.toString();
  try{m+=s1.split(".")[1].length}catch(e){}
  try{m+=s2.split(".")[1].length}catch(e){}
  return Number(s1.replace(".",""))*Number(s2.replace(".",""))/Math.pow(10,m);
}

//js 浮点数 除法  bug
/**
 * zn
 * @param arg1
 * @param arg2
 * @returns {number}
 */
string.floatDiv = function(arg1,arg2){
  var t1=0,t2=0,r1,r2;
  try{t1=arg1.toString().split(".")[1].length}catch(e){}
  try{t2=arg2.toString().split(".")[1].length}catch(e){}

  r1=Number(arg1.toString().replace(".",""));

  r2=Number(arg2.toString().replace(".",""));
  return (r1/r2)*Math.pow(10,t2-t1);
}

/**
 * 科学计数法转换
 * @param value
 * @returns {*}
 */
string.scienceNum=function(value) {
  if (isEmpty(value)) {
    return value;
  }
  value = value.toString();
  if (value.indexOf("e") > 0 || value.indexOf("E") > 0) {
    var ePart;
    var scalePart;
    var valueList;
    if (value.indexOf("e") > 0) {
      valueList = value.split("e");
    } else if (value.indexOf("E") > 0) {
      valueList = value.split("E");
    }
    if (valueList.length < 2) {
      return value;
    }
    ePart = valueList[0];
    scalePart = valueList[1];
    var result;
    if (ePart.indexOf(".") > 0) {
      var ePartList = ePart.split(".");
      var fLength = ePartList[0].length;
      var sLength = ePartList[1].length;
      if (scalePart.indexOf("-") >= 0) {
        if (ePartList[0].indexOf("-") >= 0) {
          ePartList[0] = ePartList[0].replace("-", "");
          result = "-0.";
        } else {
          result = "0.";
          ePartList[0] = ePartList[0].replace("+", "");
        }
        var scalePartList = scalePart.split("-");
        for (var i = 0; i < scalePartList[1] - 1; i++) {
          result = result + "0";
        }
        result = result + ePartList[0] + ePartList[1];
        return result;
      } else {
        var scalePartList;
        if (scalePart.indexOf("+") >= 0) {
          scalePartList = scalePart.split("+");
          scalePartList = scalePartList[1];
        } else {
          scalePartList = scalePart;
        }
        ePartList[0] = ePartList[0].replace("+", "");
        result = ePartList[0].toString() + ePartList[1].toString();
        var leftLength = scalePartList - sLength;
        if (leftLength > 0) {
          for (var j = 0; j < leftLength; j++) {
            result = result + "0";
          }
        }
        return result;

      }
    } else {
      if (scalePart.indexOf("-") >= 0) {
        if (ePart.indexOf("-") >= 0) {
          ePart = ePart.replace("-", "");
          result = "-0.";
        } else {
          ePart = ePart.replace("+", "");
          result = "0.";
        }
        var scalePartList = scalePart.split("-");
        for (var i = 0; i < scalePartList[1] - 1; i++) {
          result = result + "0";
        }
        result = result + ePart;
        return result;
      } else {
        var scalePartList;
        if (scalePart.indexOf("+") >= 0) {
          scalePartList = scalePart.split("+");
          scalePartList = scalePartList[1];
        } else {
          scalePartList = scalePart;
        }
        ePart = ePart.replace("+", "");
        result = ePart.toString();
        var leftLength = scalePartList;
        if (leftLength > 0) {
          for (var j = 0; j < leftLength; j++) {
            result = result + "0";
          }
        }
        return result;

      }
    }

  }
  return value;
}

/**zxd
 * 按照开头首字母排序
 * @param str 传入为字符串类型或者数组
 */
string.characterSort = function(str){
  if(typeof str === 'string')
    str = str.split(',');
  if (!Array.isArray(str)) {
    console.error('参数类型错误, 必须为数组或以(,)分割的字符串.')
    return str;
  }
  str.sort();
  return str.join();
}

/**zxd
 * @param ElementId 传入的dom元素的id, 即：被复制的内容dom标签Id
 * @callback 回调函数 true为复制成功 false为复制不成功
 */
string.copyToClipboard = function (ElementId,callback) {

  try {
    // 获取复制内容
    var content = document.getElementById(ElementId).innerHTML || document.getElementById(ElementId).value;
    if(!content){
      callback(false)
    }
    if (window.clipboardData) {
      window.clipboardData.clearData();
      clipboardData.setData("Text", content);
    } else if (navigator.userAgent.indexOf("Opera") != -1) {
      window.location = content;
    }else{
      // 创建元素用于复制
      var aux = document.createElement("input");
      // 设置元素内容
      aux.setAttribute("value", content);
      // 将元素插入页面进行调用
      document.body.appendChild(aux);
      // 复制内容
      aux.select();
      // 将内容复制到剪贴板
      document.execCommand("copy");
      // 删除创建元素
      document.body.removeChild(aux);
    }
    callback(true)
  }catch (e) {
    callback(false)
  }
}

//解析url值
/**yt
 * @param ElementId 传入参数名name 获取对应的值
 */
string.getQueryString = function(name){
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
  var r = window.location.search.substr(1).match(reg);
  if (r != null) return unescape(r[2]); return null;
}



string.getTimeFormat = function(value,type) {
  if (value) {
    let oDate = new Date();
    oDate.setTime(value);
    let y = oDate.getFullYear();
    let m = oDate.getMonth() + 1;
    let d = oDate.getDate();

    let h = oDate.getHours();
    let mm = oDate.getMinutes();
    let s = oDate.getSeconds();
    if(type=="long"){
      return y + '-' + checkTime(m) + '-' + checkTime(d) + ' ' + checkTime(h) + ':' + checkTime(mm) + ':' + checkTime(s);
    }else{
      return y + '.' + checkTime(m) + '.' + checkTime(d);
    }
  }
}

function checkTime(num){
  if(num<10){
    return "0"+num
  }else{
    return num
  }
}

/**
 *  //计算两个时间相差了几个小时
 * @param startDate
 * @param endDate
 * @returns {*}
 */
string.getIntervalHour = function(startDate, endDate) {
  var ms = endDate.getTime() - startDate.getTime();
  if (ms < 0) return 0;
  return Math.floor(ms/1000/60/60);
}

export default  string;
