
var initChart=function(arr,idContainer) {
  var total = 0;
  var average = 0;
  var cv = document.getElementById(idContainer);
  cv.width = 156;
  cv.height = 50;
  cv.lineWidth = 1;
  // cv.style.border = "1px solid red";
  var ctx = cv.getContext("2d");

  if(arr !== null && arr !== undefined){
    for(var j=0;j<arr.length;j++){
      total+=arr[j]/1;
    }
    average = total/(arr.length);
    getBrokenLine(arr, "#00c1de");
  }


  //封装一个折线图的函数
  function getBrokenLine(data, color) {
    var maxNum = Math.max.apply(null, data);    //求数组中的最大值
    var minNum = Math.min.apply(null, data);    //求数组中的最小值
    var padding = 5,  //边距
    xLength = cv.width - 2*padding ,    //x轴的长度
    yLength = cv.height - 2*padding,  //y轴的长度
    pointsWidth = xLength/(data.length + 1);    //折线上每个点之间的距离

    ctx.beginPath();//控制绘制的折线不受坐标轴样式属性的影响

    //绘制折线
    for (var i = 0; i < data.length; i++) {
      var pointX = padding + (i + 1) * pointsWidth;
      var pointY = padding  + (1-(data[i]-minNum)/(maxNum-minNum)) * yLength;
      if(minNum === maxNum){
        pointY = padding +yLength/2;
      }else if(maxNum < average * 1.01){
        pointY = 20  + (1-(data[i]-minNum)/(maxNum-minNum)) * yLength/4;
      }
      ctx.lineTo(pointX, pointY);
    }
    ctx.strokeStyle = color;
    ctx.stroke();
  }

}

export default initChart
