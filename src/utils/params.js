var params = function(){};
/**
 * lxz
 * @param language
 * @returns {number}
 */
params.languages = function(language){
  if(language === "zh_CN"){
    return  0;
  }else if(language === "en"){
    return  1;
  }else if(language === "ja"){
    return  2;
  }else if(language === 'th'){
    return  3;
  }else if(language === 'vi'){
    return  4;
  }
}


export default params;
