import storage from '../src/utils/storage.js'
var udesk = function(){
  var deskLang;
  var serviceLanguage = "";
  var langForDesk = storage.getCookie("Language");
  if(langForDesk === "zh_CN"){
    deskLang = 'zh-cn'
    serviceLanguage = "在线咨询";
  }else if(langForDesk === "ja"){
    deskLang = 'ja'
    serviceLanguage = 'お問い合わせ';
  }else if(langForDesk === 'th'){
    deskLang = 'th'
    serviceLanguage = 'บริการออนไลน์';
  }else if(langForDesk === 'vi'){
    deskLang = 'vi'
    serviceLanguage = 'Dịch vụ tư vấn khách hàng trên mạng';
  }else if(langForDesk === 'en'){
    deskLang = 'en-us'
    serviceLanguage = 'Support';
  }

  !(function(a,h,c,b,f,g){a["UdeskApiObject"]=f;a[f]=a[f]||function(){(a[f].d=a[f].d||[]).push(arguments)};g=h.createElement(c);g.async=1;g.charset="utf-8";g.src=b;c=h.getElementsByTagName(c)[0];c.parentNode.insertBefore(g,c)})(window,document,"script","//assets-cli.udesk.cn/im_client/js/udeskApi.js","ud");
    ud({
      "manualInit":true,
      "onReady":function(){
        ud.init();
        document.getElementById("udesk_btn_text").innerHTML = serviceLanguage;
      },
      "code": "22kda629",
      "language":deskLang,
      "onlineText":serviceLanguage,
      "link": "//15898536553.udesk.cn/im_client/?web_plugin_id=44439"
    });

}
export default udesk;
